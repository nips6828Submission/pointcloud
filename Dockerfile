from registry.gitlab.com/nips6828submission/pcl as pcl

from registry.gitlab.com/nips6828submission/base
MAINTAINER nips6828@gmail.com
env NVIDIA_VISIBLE_DEVICES=void
copy --from=pcl / /usr
run apt-get update -y
RUN apt-get install -y libeigen3-dev
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y libvtk7-qt-dev
RUN apt-get install -y libflann-dev
run apt-get install meshlab -y
run apt-get install -y xvfb
run apt-get install -y parallel
run apt-get install -y software-properties-common
run apt-get install -y git build-essential linux-libc-dev
run apt-get install -y cmake cmake-gui 
workdir /workspace/PointCloud
copy PointCloud . 
run mkdir -p build
workdir /usr/share/pcl-1.9
workdir /workspace/PointCloud/build
run ldconfig
run cmake -DCMAKE_BUILD_TYPE=Release ..
run make -j $(nproc)
run mkdir -p /Data/Data/
copy PointCloudData /Data/Data/PointCloud 
env NVIDIA_VISIBLE_DEVICES=all
