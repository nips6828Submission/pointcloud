To execute without regularizer:
``` bash
./MainRandomizeRnnExtracted
```

To execute with the regularizer:
``` bash
./MainRandomizeRnnRegularizedExtracted
```

To get help without regularizer:
``` bash
./MainRandomizeRnnExtracted --help
```

To get help with regularizer:
``` bash
./MainRandomizeRnnRegularizedExtracted --help
```