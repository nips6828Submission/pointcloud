#!/usr/bin/env bash
#The first part of this file fix a "bug" in modelnet40 data where there is a missing new line on some files.
#The second part preprocess the file to the ply format.
header=$(head -n 1 $1)
if [ "$header" == "OFF" ]
then
    :
else
    subheader=${header:0:3}
    if [ "$subheader" == "OFF" ]
    then
        rest=${header:3}
        newline="OFF\n$rest"
        sed -i "1s/.*/$newline/" $1
    fi
fi
xvfb-run -a -s "-screen 0 800x600x24" meshlabserver -i $1 -o $1.ply
