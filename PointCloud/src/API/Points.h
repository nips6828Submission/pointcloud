#pragma once
#include <vtkPoints.h>
#include "PointId.h"
#include "Point3d.h"
#include <vtkSmartPointer.h>
#include <exception>
#include <stdexcept>
#include "Core/ExpressionGenerator.h"

struct Points
{
    vtkSmartPointer<vtkPoints> m_pts;
};

template <>
class ML<Points>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    typedef Points Type;
};

size_t ML<Points>::m_size=256;

Points Points_SetNumberOfPoints(Points points,PointId number)
{
    points.m_pts->SetNumberOfPoints(static_cast<vtkIdType>(number));
    return points;
}

PointId Points_GetNumberOfPoints(Points points)
{
    return points.m_pts->GetNumberOfPoints();
}

Point3d Points_GetPoint(Points points,PointId number)
{
    PointId NB=points.m_pts->GetNumberOfPoints();
    if(static_cast<vtkIdType>(number)<static_cast<vtkIdType>(NB))
    {
        double point[3];
        points.m_pts->GetPoint(static_cast<vtkIdType>(number)%points.m_pts->GetNumberOfPoints(),point);
        Point3d pointres{point[0],point[1],point[2]};
        return pointres;
    }
    else if(static_cast<vtkIdType>(NB)>0)
    {
        throw std::runtime_error(std::string("Id not existing"));
    }
    else
    {
        throw std::runtime_error(std::string("No possible value"));
    }
}

template <typename TypeGenerator,typename TypeExpression>
std::function<std::tuple<Points,PointId>(std::tuple<Points,PointId>,TypeGenerator&,TypeExpression&)> Points_GetPointGen()
{
    std::function<std::tuple<Points,PointId>(std::tuple<Points,PointId>,TypeGenerator&,TypeExpression&)> func=[](std::tuple<Points,PointId> tuple,TypeGenerator& G,TypeExpression& expression)
    {
        vtkIdType nb=std::get<0>(tuple).m_pts->GetNumberOfPoints();
        if(nb==0)
        {
            throw std::runtime_error(std::string("No possible value"));
        }
        vtkIdType id=static_cast<vtkIdType>(std::get<1>(tuple));
        if(id<nb)
        {
            return tuple;
        }
        else
        {
            std::uniform_int_distribution<> distribution(0,nb-1);
            int val=distribution(G);
            std::get<1>(tuple)=val;
            expression.m_inputs[1].Set(PointId(val));
            return tuple;
        }
    };
    return func;
};

Points Points_SetPoint(Points points,PointId number,Point3d point)
{
    if(points.m_pts->GetNumberOfPoints()<=static_cast<vtkIdType>(number))
    {
        points.m_pts->SetNumberOfPoints(static_cast<vtkIdType>(number)+1);
    }
    points.m_pts->SetPoint(static_cast<vtkIdType>(number),point.x,point.y,point.z);
    return points;
}

Points Points_InsertPoint(Points points,PointId number,Point3d point)
{
    points.m_pts->SetPoint(static_cast<vtkIdType>(number),point.x,point.y,point.z);
    return points;
}

Points Points_InsertNextPoint(Points points,Point3d point)
{
    points.m_pts->InsertNextPoint(point.x,point.y,point.z);
    return points;
}

Points PointsConstructor()
{
    Points points;
    points.m_pts=vtkSmartPointer<vtkPoints>::New();
    return points;
}
