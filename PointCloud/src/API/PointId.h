#pragma once
#include <vtkPoints.h>
#include "Core/ML.h"
#include <climits>
#include <bitset>

struct PointId
{
    vtkIdType id;
    PointId()
    {
    }
    explicit operator vtkIdType()
    {
        return id;
    }
    PointId(vtkIdType in_id):id(in_id)
    {
    }
};

template <>
class ML<PointId>: public at::Tensor
{
    public:
    constexpr static size_t size()
    {
        return sizeof(vtkIdType)*CHAR_BIT;
    }
    ML()
    {
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
    }
    typedef PointId Type;
};

auto GenerateMLValue(PointId res,at::Tensor tensor)
{
    std::bitset<ML<PointId>::size()> bytes(res.id);
    for(int i=0;i<ML<PointId>::size();++i)
    {
        bool bit=bytes[i];
        tensor[i]=double(bit);
    }
    return ML<PointId>(tensor);
};

template <typename Accessor>
void GenerateMLValue(PointId res,Accessor tensor)
{
    std::bitset<ML<PointId>::size()> bytes(res.id);
    for(int i=0;i<ML<PointId>::size();++i)
    {
        bool bit=bytes[i];
        tensor[i]=double(bit);
    }
};
