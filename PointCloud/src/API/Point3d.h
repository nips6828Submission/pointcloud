#pragma once
#include "Core/ML.h"

struct Point3d
{
    double x;
    double y;
    double z;
};

template <>
class ML<Point3d>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    typedef Point3d Type;
};

size_t ML<Point3d>::m_size=3;

/*template<typename Type>
auto GenerateMLValue(pcl::PointXYZ res,Type& device)
{
    auto var=at::zeros({ML<pcl::PointXYZ>::size()},device);
    var[0]=res.x;
    var[1]=res.y;
    var[2]=res.z;
    var[3]=res.x*res.x;
    var[4]=res.y*res.y;
    var[5]=res.z*res.z;
    return ML<pcl::PointXYZ>(torch::autograd::make_variable(var,false));
};
*/

auto GenerateMLValue(Point3d res,at::Tensor tensor)
{
    tensor[0]=res.x;
    tensor[1]=res.y;
    tensor[2]=res.z;
    return ML<Point3d>(tensor);
};

template <typename Accessor>
void GenerateMLValue(Point3d res,Accessor tensor)
{
    tensor[0]=res.x;
    tensor[1]=res.y;
    tensor[2]=res.z;
};
