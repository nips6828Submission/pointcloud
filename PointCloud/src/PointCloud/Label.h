#pragma once

#include "torch/csrc/autograd/variable.h"
#include "ATen/ATen.h"
#include <map>
#include <string>
#include "Core/ML.h"
#include "Core/GenerateMLValue.h"

class Label
{
    size_t m_id;
public:
    Label(size_t id):m_id(id)
    {
    }
    Label()
    {
    }
    size_t GetId() const
    {
        return m_id;
    }
    std::string GetName() const
    {
        return IdToName[m_id];
    }
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & m_id;
    }
    static size_t size;
    friend bool operator<(const Label& G1,const Label& G2);
    static std::map<std::string,size_t> NameToId;
    static std::map<size_t,std::string> IdToName;
};

std::map<std::string,size_t> Label::NameToId;
std::map<size_t,std::string> Label::IdToName;

size_t Label::size=-1;

bool operator<(const Label& G1,const Label& G2)
{
    return G1.m_id<G2.m_id;
}

template <typename Type>
auto GenerateMLValue(const Label& res,Type& device)
{
    at::Tensor ten=at::zeros({Label::size,1},device);
    ten[res.GetId()]=1;
    return ML<Label>(torch::autograd::make_variable(ten,false));
};

template <>
class ML<Label>: public at::Tensor
{
    public:
    static size_t size()
    {
        return Label::size;
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
	return false;
    }
    typedef Label Type;
};

auto GenerateMLValue(const Label& res,at::Tensor tensor)
{
    tensor[res.GetId()]=1;
    return ML<Label>(tensor);
};

