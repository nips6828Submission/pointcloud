#pragma once
#include <pcl/impl/point_types.hpp>

template <>
class ML<pcl::PointXYZ>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
        return false;
    }
    typedef pcl::PointXYZ Type;
};

size_t ML<pcl::PointXYZ>::m_size=6;

/*template<typename Type>
auto GenerateMLValue(pcl::PointXYZ res,Type& device)
{
    auto var=at::zeros({ML<pcl::PointXYZ>::size()},device);
    var[0]=res.x;
    var[1]=res.y;
    var[2]=res.z;
    var[3]=res.x*res.x;
    var[4]=res.y*res.y;
    var[5]=res.z*res.z;
    return ML<pcl::PointXYZ>(torch::autograd::make_variable(var,false));
};
*/

auto GenerateMLValue(pcl::PointXYZ res,at::Tensor tensor)
{
    tensor[0]=res.x;
    tensor[1]=res.y;
    tensor[2]=res.z;
    tensor[3]=res.x*res.x;
    tensor[4]=res.y*res.y;
    tensor[5]=res.z*res.z;
    return ML<pcl::PointXYZ>(tensor);
};

template <typename Accessor>
void GenerateMLValue(pcl::PointXYZ res,Accessor tensor)
{
    tensor[0]=res.x;
    tensor[1]=res.y;
    tensor[2]=res.z;
    tensor[3]=res.x*res.x;
    tensor[4]=res.y*res.y;
    tensor[5]=res.z*res.z;
};
