#pragma once
#include "Label.h"
#include "Cloud.h"
#include "Core/Sample.h"
#include <mutex>

struct DataMesh
{
    static std::mutex mutex_polyData;
    Label label;
    std::string file;
    vtkSmartPointer<vtkPolyData> polydata;
    size_t nbSample;
    public:
    Cloud Sample() const
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
        ::Sample(polydata,cloud,nbSample);
        return Cloud(cloud,label);
    }
    
    void SetPolyData()
    {
        polydata=GetPolyData(file);
    }
    
    template<class Archive>
    void serialize(Archive & ar, const unsigned int version)
    {
        ar & label;
        ar & file;
        ar & nbSample;
    }
};

std::mutex DataMesh::mutex_polyData;
