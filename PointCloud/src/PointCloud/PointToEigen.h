
Eigen::Vector4f ToEigen(pcl::PointXYZ point)
{
    return Eigen::Vector4f(point.x,point.y,point.z,0.0);
}
