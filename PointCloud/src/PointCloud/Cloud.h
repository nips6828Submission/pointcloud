#pragma once

class Cloud
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr m_points;
    Label m_label;
public:
    Cloud()=default;
    Cloud(pcl::PointCloud<pcl::PointXYZ>::Ptr points,const Label& label):m_points(points),m_label(label)
    {
    }
    void SetLabel(const Label& label)
    {
        m_label=label;
    }
    Label GetLabel() const
    {
        return m_label;
    }
    Label& GetLabelRef()
    {
        return m_label;
    }
    void SetPoints(pcl::PointCloud<pcl::PointXYZ>::Ptr points)
    {
        m_points=points;
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr GetPoints() const
    {
        return m_points;
    }
    pcl::PointCloud<pcl::PointXYZ>::Ptr& GetPointsRef()
    {
        return m_points;
    }
};

template <>
class ML<Cloud>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
        return false;
    }
    typedef Cloud Type;
};

size_t ML<Cloud>::m_size=1024;

template<typename Type>
auto GenerateMLValue(Cloud res,Type& device)
{
    return ML<Cloud>(torch::autograd::make_variable(at::zeros({ML<Cloud>::size(),1},device),false));
};

auto GenerateMLValue(Cloud res,at::Tensor tensor)
{
    return ML<Cloud>(tensor);
};


