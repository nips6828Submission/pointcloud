#pragma once

class HiddenPoint
{
};

template <>
class ML<HiddenPoint>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
        
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
        return false;
    }
    typedef HiddenPoint Type;
};

size_t ML<HiddenPoint>::m_size=1024;
