#include <Core/StartUp.h>
#define SERIALIZATIONS BOOST_SERIALIZATIONS

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/filesystem.hpp>
#include <boost/serialization/deque.hpp>

#include <iostream>
#include <algorithm>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/common/transforms.h>
#include <vtkVersion.h>
#include <vtkTriangle.h>
#include <vtkTriangleFilter.h>
#include <vtkPolyDataMapper.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/ply_io.h>
#include <boost/program_options.hpp>

#include "Core/Sample.h"
#include "Core/Backward.h"

#include  "Learning/ExperimentDoubleRnnExtracted.h"
#include "Learning/RandomizeRnnDouble.h"
#include "Learning/RandomizeRnnDoubleRegularizedExtracted.h"

#include "torch/csrc/api/include/torch/optim/adam.h"
#include "torch/csrc/api/include/torch/optim/sgd.h"
#include <future>
#include <nvToolsExt.h>
#include <csignal>

StartUp start;

constexpr size_t nbThread=6;

std::atomic<bool> quit(false);

void signal_handler(int)
{
    quit.store(true);
}

int main(int argc, char *argv[])
{
    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM,signal_handler);
    std::signal(SIGABRT,signal_handler);
    int nn_depth;
    int nn_hidden;
    int lstm_hidden;
    bool randomize;
    size_t sample;
    std::string NameData;
    std::string Name;
    int nbEpoch;
    size_t batchSize;
    double learningRate;
    size_t nbVal;
    size_t SaveEvery;
    bool curiculum=false;
    int layerExtracted;
    int hiddenExtracted;
    boost::program_options::options_description desc("Allowed options");
    desc.add_options()
    ("help", "produce help message")
    ("nnDepth",boost::program_options::value<int>(&nn_depth)->default_value(1),"Depth of the NN network predicting longitude and lattitude")
    ("nnHidden",boost::program_options::value<int>(&nn_hidden)->default_value(128),"Number of hidden unit of the NN network predicting longitude and lattitude")
    ("lstmHidden",boost::program_options::value<int>(&lstm_hidden)->default_value(128),"Number of hidden unit of the lstm")
    ("randomize",boost::program_options::value<bool>(&randomize)->default_value(true),"If we randomize the LSTM.")
    ("sample",boost::program_options::value<size_t>(&sample)->default_value(100),"How many points we sample per object.")
    ("name",boost::program_options::value<std::string>(&Name)->default_value("Default"),"Name of the experiment Run")
    ("nameData",boost::program_options::value<std::string>(&NameData)->default_value("ModelNet10"),"Name of the Data Split to use")
    ("nbEpoch",boost::program_options::value<int>(&nbEpoch)->default_value(500),"Number of epoch to use.")
    ("learningRate",boost::program_options::value<double>(&learningRate)->default_value(1e-3),"Learning rate.")
    ("batchSize",boost::program_options::value<size_t>(&batchSize)->default_value(100),"Batch Size.")
    ("nbVal",boost::program_options::value<size_t>(&nbVal)->default_value(100),"Number of sample to use for validation for every class.")
    ("saveEvery",boost::program_options::value<size_t>(&SaveEvery)->default_value(10),"Every how many epoch to save the model.")
    ("curiculum",boost::program_options::value<bool>(&curiculum)->default_value(false),"Should we do curriculum learning.")
    ("layerExtracted",boost::program_options::value<int>(&layerExtracted)->default_value(1),"Number of layer for the feature extractor.")
    ("hiddenExtracted",boost::program_options::value<int>(&hiddenExtracted)->default_value(64),"Number of hidden state for the feature extractor.")
    ("check","check if the script was already run.");
    
    boost::program_options::variables_map vm;

    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
    
    boost::program_options::notify(vm);
        
    if (vm.count("help"))
    {
        std::cout << desc << "\n";
        return 0;
    }
    
    if(nbEpoch<=0)
    {
        throw std::runtime_error("Number of epoch cannot be negative or zero.");
    }
    if(nn_depth<0 || nn_hidden<=0 || lstm_hidden<=0 )
    {
        throw std::runtime_error("The depth or number of unit canot be negatif or zero.");
    }
    ExperimentDoubleRnnExtracted Exp(nn_depth,nn_hidden,lstm_hidden,sample,Name,NameData,nbVal,learningRate,curiculum,true,layerExtracted,hiddenExtracted);
    if(Exp.AlreadyDone())
    {
        std::cout<<"Already done"<<std::endl;
        return 0;
    }
    else
    {
        if(vm.count("check"))
        {
            return 1;
        }
    }
    nvtxRangePushA("Loading data");
    auto expData=Exp.GetData();
    expData.Load();
    nvtxRangePop();
    
    boost::filesystem::create_directories(boost::filesystem::path{Exp.GetDirectoryPath()});
    std::ofstream ofs(Exp.GetDirectoryPath()+std::string("/Experiment.txt"));
    std::cout<<"Result written to "<<Exp.GetDirectoryPath()+std::string("/Experiment.txt")<<std::endl;
    
    auto& DataTrain=expData.TrainData();
    
    auto& DataTest=expData.TestData();
    
    auto& DataVal=expData.ValData();

    ML<Cloud>::m_size=lstm_hidden;
    ML<HiddenPoint>::m_size=hiddenExtracted;
    typedef RandomizeRnnDoubleRegularizedExtracted Compute;
    
    nvtxRangePushA("Creating graph");
    std::vector<size_t> hidden_Form(nn_depth,nn_hidden);
    std::vector<size_t> hidden_extracted(layerExtracted,hiddenExtracted);
    Compute Graph(hidden_Form,hidden_extracted);
    nvtxRangePop();
    
    auto param=Graph.GetTensor();
    auto adamOption=torch::optim::AdamOptions(learningRate);
    torch::optim::Adam adam(param,adamOption);
    
    std::chrono::steady_clock clock;
    typedef std::chrono::duration<double> duration_t;
    size_t nbBatch=(DataTrain.size()-1)/batchSize+1;
    size_t nbBatchTest=(DataTest.size()-1)/batchSize+1;
    size_t nbBatchVal=(DataVal.size()-1)/batchSize+1;
    if(DataVal.size()==0)
    {
        nbBatchVal=0;
    }
    
    auto funcFill=[&Graph,&DataTrain,&batchSize,&curiculum,&sample](size_t batch,size_t epoch)
    {
        size_t start=batch*batchSize;
        size_t last=std::min((batch+1)*batchSize,DataTrain.size());
        size_t length;
        if(curiculum)
        {
            length=4*epoch+1;
        }
        else
        {
            length=sample;
        }
        return Graph.ToBatchTensor(DataTrain.begin()+start,DataTrain.begin()+last,length);
    };
    
    auto funcFillTest=[&Graph,&DataTest,&batchSize,&curiculum,&sample](size_t batch,size_t epoch)
    {
        size_t start=batch*batchSize;
        size_t last=std::min((batch+1)*batchSize,DataTest.size());
        size_t length;
        if(curiculum)
        {
            length=4*epoch+1;
        }
        else
        {
            length=sample;
        }
        return Graph.ToBatchTensor(DataTest.begin()+start,DataTest.begin()+last,length);
    };
    
    auto funcFillVal=[&Graph,&DataVal,&batchSize,&curiculum,&sample](size_t batch,size_t epoch)
    {
        size_t start=batch*batchSize;
        size_t last=std::min((batch+1)*batchSize,DataVal.size());
        size_t length;
        if(curiculum)
        {
            length=4*epoch+1;
        }
        else
        {
            length=sample;
        }
        return Graph.ToBatchTensor(DataVal.begin()+start,DataVal.begin()+last,length);
    };
    int i=1;
    std::queue<std::future<torch::jit::Stack>> queue;
    auto FillQueue=[&]()
    {
        for(size_t batch=0;batch<std::min(size_t(nbThread),nbBatch);++batch)
        {
            auto input=std::async(std::launch::async,funcFill,batch,i);
            queue.push(std::move(input));
        }
    };
    FillQueue();
    
    std::queue<std::future<torch::jit::Stack>> queueTest;
    auto FillQueueTest=[&]()
    {
        for(size_t batch=0;batch<std::min(size_t(nbThread),nbBatchTest);++batch)
        {
            auto input=std::async(std::launch::async,funcFillTest,batch,i);
            queueTest.push(std::move(input));
        }
    };
    FillQueueTest();
    
    std::queue<std::future<torch::jit::Stack>> queueVal;
    auto FillQueueVal=[&]()
    {
        for(size_t batch=0;batch<std::min(size_t(nbThread),nbBatchVal);++batch)
        {
            auto input=std::async(std::launch::async,funcFillVal,batch,i);
            queueVal.push(std::move(input));
        };
    };
    FillQueueVal();
    
    for(i=1;i<=nbEpoch;++i)
    {
        nvtxRangePushA("Epoch"); 
        double error_train;
        double error_test;
        double error_val;
        
        double acc_train;
        double acc_test;
        double acc_val;
        
        double error_train_last;
        double error_test_last;
        double error_val_last;
        
        double timePerInstance;
        
        double GradNorm;
        {
        nvtxRangePushA("Train");
        auto start_time = clock.now();
        double error=0;
        double weightNorm=0;
        double acc=0;
        size_t batch=std::min(size_t(nbThread),nbBatch);
        while(batch<nbBatch)
        {
            adam.zero_grad();
            torch::autograd::Variable one=torch::autograd::make_variable(at::ones({1},at::kCUDA),false);
            nvtxRangePushA("GetInput");
            auto realInput=queue.front().get();
            nvtxRangePop();
            queue.pop();
            auto inputnew=std::async(std::launch::async,funcFill,batch,i);
            queue.push(std::move(inputnew));
            nvtxRangePushA("Forward"); 
            auto [errorVar,errorVarNotNorm,varacc]=Graph.Execute(realInput);
            nvtxRangePop();
            nvtxRangePushA("Backward");
            Backward(errorVar,one);
            nvtxRangePop();
            nvtxRangePushA("Clipping");
            std::for_each(param.begin(),param.end(),[&weightNorm](auto elem)
            {
                if(elem.grad().defined())
                {
                    elem.grad().detach_();
                    elem.grad().clamp_(-5,5);
                    torch::autograd::Variable normWeight=elem.grad().norm();
                    weightNorm=std::max(normWeight.item<double>(),weightNorm);
                }
            });
            nvtxRangePop();
            adam.step();
            error+=errorVarNotNorm.item<double>();
            acc+=varacc.item<double>();
            ++batch;
        }
        while(!queue.empty())
        {
            adam.zero_grad();
            torch::autograd::Variable one=torch::autograd::make_variable(at::ones({1},at::kCUDA),false);
            nvtxRangePushA("GetInput");
            auto realInput=queue.front().get();
            nvtxRangePop();
            queue.pop();
            nvtxRangePushA("Forward");
            auto [errorVar,errorVarNotNorm,varacc]=Graph.Execute(realInput);
            nvtxRangePop();
            nvtxRangePushA("Backward");
            Backward(errorVar,one);
            nvtxRangePop();
            nvtxRangePushA("Clipping");
            std::for_each(param.begin(),param.end(),[&weightNorm](auto elem)
            {
                if(elem.grad().defined())
                {
                    elem.grad().detach_();
                    elem.grad().clamp_(-5,5);
                    torch::autograd::Variable normWeight=elem.grad().norm();
                    weightNorm=std::max(normWeight.item<double>(),weightNorm);
                }
            });
            nvtxRangePop();
            adam.step();
            error+=errorVarNotNorm.item<double>();
            acc+=varacc.item<double>();
            ++batch;
        }
        nvtxRangePop();
        std::cout<<"epoch "<<i<<std::endl;
        duration_t difftime=clock.now()-start_time;
        std::cout<<"data size "<<DataTrain.size()<<std::endl;
        std::cout<<"total time "<<difftime.count()<<std::endl;
        timePerInstance=difftime.count()/(DataTrain.size());
        std::cout<<"time per instance "<<timePerInstance<<std::endl;
        std::cout<<"Error "<<error/static_cast<double>(DataTrain.size())<<std::endl;
        error_train=error/static_cast<double>(DataTrain.size());
        acc_train=acc/static_cast<double>(DataTrain.size());
        std::cout<<"Accuracy "<<acc_train<<std::endl;
        std::cout<<"Grad WeightNorm "<<weightNorm<<std::endl;
        GradNorm=weightNorm;
        std::random_shuffle(DataTrain.begin(),DataTrain.end());
        nvtxRangePushA("FillQueue");
        FillQueue();
        nvtxRangePop();
        }
        {
        nvtxRangePushA("Test");
        auto start_time = clock.now();
        size_t batch=std::min(size_t(nbThread),nbBatchTest);
        double errortest=0;
        double acc=0;
        while(batch<nbBatchTest)
        {
            auto realInput=queueTest.front().get();
            queueTest.pop();
            auto inputnew=std::async(std::launch::async,funcFillTest,batch,i);
            queueTest.push(std::move(inputnew));
            auto [errorVar,errorVarNotNorm,accvar]=Graph.Execute(realInput);
            errortest+=errorVarNotNorm.item<double>();
            acc+=accvar.item<double>();
            ++batch;
        }
        while(!queueTest.empty())
        {
            auto realInput=queueTest.front().get();
            queueTest.pop();
            auto [errorVar,errorVarNotNorm,accvar]=Graph.Execute(realInput);
            errortest+=errorVarNotNorm.item<double>();
            acc+=accvar.item<double>();
        }
        duration_t difftime=clock.now()-start_time;
        nvtxRangePop();
        std::cout<<"data size "<<DataTest.size()<<std::endl;
        std::cout<<"total time "<<difftime.count()<<std::endl;
        std::cout<<"Error Test "<<errortest/static_cast<double>(DataTest.size())<<std::endl;
        error_test=errortest/static_cast<double>(DataTest.size());
        acc_test=acc/static_cast<double>(DataTest.size());
        std::cout<<"Accuracy Test "<<acc_test<<std::endl;
        FillQueueTest();
        }
        
        {
        nvtxRangePushA("Val");
        auto start_time = clock.now();
        size_t batch=std::min(size_t(nbThread),nbBatchVal);
        double errorVal=0;
        double accVal=0;
        while(batch<nbBatchVal)
        {
            auto realInput=queueVal.front().get();
            queueVal.pop();
            auto inputnew=std::async(std::launch::async,funcFillVal,batch,i);
            queueVal.push(std::move(inputnew));
            auto [errorVar,errorVarNotNorm,accval]=Graph.Execute(realInput);
            errorVal+=errorVarNotNorm.item<double>();
            accVal+=accval.item<double>();
            ++batch;
        }
        while(!queueVal.empty())
        {
            auto realInput=queueVal.front().get();
            queueVal.pop();
            auto [errorVar,errorVarNotNorm,accval]=Graph.Execute(realInput);
            errorVal+=errorVarNotNorm.item<double>();
            accVal+=accval.item<double>();
        }
        duration_t difftime=clock.now()-start_time;
        nvtxRangePop();
        std::cout<<"data size "<<DataVal.size()<<std::endl;
        std::cout<<"total time "<<difftime.count()<<std::endl;
        std::cout<<"Error Val "<<errorVal/static_cast<double>(DataVal.size())<<std::endl;
        error_val=errorVal/static_cast<double>(DataVal.size());
        acc_val=accVal/static_cast<double>(DataVal.size());
        std::cout<<"Accuracy Val "<<acc_val<<std::endl;
        FillQueueVal();
        }
        nvtxRangePushA("Saving To Disk");
        ofs<<i<<" "<<error_train<<" "<<error_test<<" "<<error_val<<" "<<GradNorm<<" "<<acc_train<<" "<<acc_test<<" "<<acc_val<<" "<<timePerInstance<<" "<<9+i<<std::endl;
        if(i%SaveEvery==0)
        {
        torch::serialize::OutputArchive OAOptimizer;
        adam.save(OAOptimizer);
        torch::serialize::OutputArchive OA;
        OA.write("Optimizer",OAOptimizer);
        auto model=Graph.Save();
        OA.write("Model",model);
        OA.save_to(Exp.GetDirectoryPath()+std::string("/")+std::to_string(i));
        }
        nvtxRangePop();
        nvtxRangePop();
        if(quit.load())
        {
            std::cout<<"Receiving stop instruction"<<std::endl;
            break;
        }
    }
    Exp.SetDone();
    {
        torch::serialize::OutputArchive OAOptimizer;
        adam.save(OAOptimizer);
        torch::serialize::OutputArchive OA;
        OA.write("Optimizer",OAOptimizer);
        auto model=Graph.Save();
        OA.write("Model",model);
        OA.save_to(Exp.GetDirectoryPath()+std::string("/FinalModel"));
    }
    std::cout<<"Execution finished"<<std::endl;
}


