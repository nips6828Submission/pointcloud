#pragma once
#include <pcl/io/vtk_lib_io.h>
#include <pcl/common/transforms.h>
#include <vtkVersion.h>
#include <vtkTriangle.h>
#include <vtkTriangleFilter.h>
#include <vtkPolyDataMapper.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/ply_io.h>

/**
 * Based on code from PCL
 * 
 **/

/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2010-2011, Willow Garage, Inc.
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */


namespace
{

    inline double uniform_deviate (int seed)
    {
        double ran = seed * (1.0 / (RAND_MAX + 1.0));
        return ran;
    }

    inline void randomPointTriangle (float a1, float a2, float a3, float b1, float b2, float b3, float c1, float c2, float c3,float r1, float r2, Eigen::Vector3f& p)
    {
        float r1sqr = std::sqrt (r1);
        float OneMinR1Sqr = (1 - r1sqr);
        float OneMinR2 = (1 - r2);
        a1 *= OneMinR1Sqr;
        a2 *= OneMinR1Sqr;
        a3 *= OneMinR1Sqr;
        b1 *= OneMinR2;
        b2 *= OneMinR2;
        b3 *= OneMinR2;
        c1 = r1sqr * (r2 * c1 + b1) + a1;
        c2 = r1sqr * (r2 * c2 + b2) + a2;
        c3 = r1sqr * (r2 * c3 + b3) + a3;
        p[0] = c1;
        p[1] = c2;
        p[2] = c3;
    }

    inline void randPSurface(vtkPolyData * polydata, std::vector<double> * cumulativeAreas, double totalArea, Eigen::Vector3f& p)
    {
        float r = static_cast<float> (uniform_deviate (rand ()) * totalArea);

        std::vector<double>::iterator low = std::lower_bound (cumulativeAreas->begin (), cumulativeAreas->end () - 1, r);
        vtkIdType el = vtkIdType (low - cumulativeAreas->begin ());

        double A[3], B[3], C[3];
        vtkIdType npts = 0;
        vtkIdType *ptIds = NULL;
        polydata->GetCellPoints (el, npts, ptIds);
        polydata->GetPoint (ptIds[0], A);
        polydata->GetPoint (ptIds[1], B);
        polydata->GetPoint (ptIds[2], C);
        float r1 = static_cast<float> (uniform_deviate (rand ()));
        float r2 = static_cast<float> (uniform_deviate (rand ()));
        randomPointTriangle (float (A[0]), float (A[1]), float (A[2]),float (B[0]), float (B[1]), float (B[2]),float (C[0]), float (C[1]), float (C[2]), r1, r2, p);
    }

    void uniform_sampling (vtkSmartPointer<vtkPolyData> polydata, size_t n_samples,   pcl::PointCloud<pcl::PointXYZRGBNormal> & cloud_out)
    {
        polydata->BuildCells ();
        vtkSmartPointer<vtkCellArray> cells = polydata->GetPolys ();

        double p1[3], p2[3], p3[3], totalArea = 0;
        std::vector<double> cumulativeAreas (cells->GetNumberOfCells (), 0);
        size_t i = 0;
        vtkIdType npts = 0, *ptIds = NULL;
        for (cells->InitTraversal (); cells->GetNextCell (npts, ptIds); i++)
        {
            polydata->GetPoint (ptIds[0], p1);
            polydata->GetPoint (ptIds[1], p2);
            polydata->GetPoint (ptIds[2], p3);
            totalArea += vtkTriangle::TriangleArea (p1, p2, p3);
            cumulativeAreas[i] = totalArea;
        }
        cloud_out.points.resize (n_samples);
        cloud_out.width = static_cast<pcl::uint32_t> (n_samples);
        cloud_out.height = 1;
        for (i = 0; i < n_samples; i++)
        {
            Eigen::Vector3f p;
            randPSurface (polydata, &cumulativeAreas, totalArea, p);
            cloud_out.points[i].x = p[0];
            cloud_out.points[i].y = p[1];
            cloud_out.points[i].z = p[2];
        }
    }
}

void Sample(std::string fileName,pcl::PointCloud<pcl::PointXYZ>::Ptr cloudResult,int nbSample)
{
    using namespace pcl;
    using namespace pcl::io;
    using namespace pcl::console;
    const float default_leaf_size = 0.01f;
    pcl::PolygonMesh mesh;
    pcl::io::loadPolygonFilePLY (fileName, mesh);
    vtkSmartPointer<vtkPolyData> polydata1 = vtkSmartPointer<vtkPolyData>::New();
    pcl::io::mesh2vtk (mesh, polydata1);
    //make sure that the polygons are triangles!
    vtkSmartPointer<vtkTriangleFilter> triangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
    triangleFilter->SetInputData(polydata1);
    triangleFilter->Update ();
    
    vtkSmartPointer<vtkPolyDataMapper> triangleMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    triangleMapper->SetInputConnection (triangleFilter->GetOutputPort ());
    triangleMapper->Update ();
    polydata1 = triangleMapper->GetInput ();

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_1 (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    uniform_sampling(polydata1, nbSample, *cloud_1);
    // Voxelgrid
    VoxelGrid<PointXYZRGBNormal> grid_;
    grid_.setInputCloud (cloud_1);
    grid_.setLeafSize (default_leaf_size, default_leaf_size, default_leaf_size);
    
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr voxel_cloud (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    grid_.filter (*voxel_cloud);
    // Strip uninitialized normals and colors from cloud:
    pcl::copyPointCloud (*voxel_cloud, *cloudResult);
}

vtkSmartPointer<vtkPolyData> GetPolyData(std::string fileName)
{
    using namespace pcl;
    using namespace pcl::io;
    using namespace pcl::console;

    pcl::PolygonMesh mesh;
    pcl::io::loadPolygonFilePLY (fileName, mesh);
    
    Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
    
    pcl::PointCloud<pcl::PointXYZ> cloud; 
    pcl::fromPCLPointCloud2(mesh.cloud, cloud);
    pcl::PointXYZ minPoint;
    pcl::PointXYZ maxPoint;
    pcl::getMinMax3D(cloud,minPoint,maxPoint);
    double Scale=1/std::max(std::max(maxPoint.x-minPoint.x,maxPoint.y-minPoint.y),std::max(maxPoint.z-minPoint.z,1e-3f));
    
    transform(0,0)=Scale;
    transform(1,1)=Scale;
    transform(2,2)=Scale;
    double translateX=-(maxPoint.x+minPoint.x)*Scale/2;
    double translateY=-(maxPoint.y+minPoint.y)*Scale/2;
    double translateZ=-(maxPoint.z+minPoint.z)*Scale/2;
    transform(0,3)=translateX;
    transform(1,3)=translateY;
    transform(2,3)=translateZ;
    pcl::transformPointCloud(cloud, cloud, transform);
    pcl::toPCLPointCloud2(cloud, mesh.cloud); 
    
    vtkSmartPointer<vtkPolyData> polydata1 = vtkSmartPointer<vtkPolyData>::New();
    pcl::io::mesh2vtk (mesh, polydata1);
    //make sure that the polygons are triangles!
    vtkSmartPointer<vtkTriangleFilter> triangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
    triangleFilter->SetInputData (polydata1);
    triangleFilter->Update ();
    
    vtkSmartPointer<vtkPolyDataMapper> triangleMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    triangleMapper->SetInputConnection (triangleFilter->GetOutputPort ());
    triangleMapper->Update ();
    polydata1 = triangleMapper->GetInput ();
    return polydata1;
}

void Sample(vtkSmartPointer<vtkPolyData> polydata,pcl::PointCloud<pcl::PointXYZ>::Ptr cloudResult,int nbSample)
{
    using namespace pcl;
    using namespace pcl::io;
    using namespace pcl::console;

    const float default_leaf_size = 0.001f;
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_1 (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    uniform_sampling(polydata, nbSample, *cloud_1);
    // Voxelgrid
    VoxelGrid<PointXYZRGBNormal> grid_;
    grid_.setInputCloud (cloud_1);
    grid_.setLeafSize (default_leaf_size, default_leaf_size, default_leaf_size);
    
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr voxel_cloud (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    grid_.filter (*voxel_cloud);
    // Strip uninitialized normals and colors from cloud:
    pcl::copyPointCloud (*voxel_cloud, *cloudResult);
}

