#pragma once
#include "RandomizeRnnDouble.h"

class RandomizeRnnDoubleEval: public RandomizeRnnDouble
{
    template <typename Iterator>
    torch::jit::Stack ToBatchTensor(Iterator begin,Iterator end,size_t epoch, bool is_train=true)=delete;
    struct DataEval
    {
        DataMesh data;
        torch::jit::Stack stack;
    };
    
    public:
    DataEval ToBatchTensor(DataMesh elem,size_t nbCopy)
    {
        nvtxRangePushA("BatchToTensor");
        std::vector<at::Tensor> Value;
        std::vector<at::Tensor> Size;
        std::vector<at::Tensor> Label;
        int nb=0;
        static size_t nbiter=0;
        ++nbiter;
        nvtxRangePushA("Generate");
        elem.SetPolyData();
        for(int i=0;i<nbCopy;++i)
        {
            nvtxRangePushA("Sample");
            auto cloud=elem.Sample();
            nvtxRangePop();
            ++nb;
            nvtxRangePushA("CreateTensors");
            auto [tenValue,tenSize,tenLabel,tenValue2]=SingleToTensor(cloud);
            nvtxRangePop();
            Value.push_back(tenValue);
            Size.push_back(tenSize);
            Label.push_back(tenLabel);
        }
        nvtxRangePop();
        nvtxRangePushA("CreateStack");
        auto StaticDim=at::ones({1},at::kByte);
        StaticDim[0]=false;
        auto DynamicDim=at::ones({2},at::kByte);
        //DynamicDim[0]=false;
        DynamicDim[0]=true;
        DynamicDim[1]=false;
        
        torch::jit::BatchTensor BatchValue(Value,DynamicDim);
        torch::jit::BatchTensor BatchSize(Size,StaticDim);
        torch::jit::BatchTensor BatchLabel(Label,StaticDim);
        auto funcVar=[](auto elem)
        {
            return torch::autograd::make_variable(elem.to(at::Device("cuda")),false);
        };
        
        torch::jit::Stack stack({funcVar(BatchValue.get_data()),funcVar(BatchValue.get_mask()),funcVar(BatchValue.get_dims()),funcVar(BatchSize.get_data()),funcVar(BatchSize.get_mask()),funcVar(BatchSize.get_dims()),funcVar(BatchLabel.get_data()),funcVar(BatchLabel.get_mask()),funcVar(BatchLabel.get_dims())});
                
        auto batchParam=[](auto elem)
        {
            torch::jit::Stack res;
            std::for_each(elem.begin(),elem.end(),[&res](auto elem2)
            {
                auto var=elem2.toTensor();
                var.set_requires_grad(false);
                torch::jit::BatchTensor batch(var,size_t(1));
                res.push_back(batch.get_data());
                res.push_back(batch.get_mask());
                res.push_back(batch.get_dims());
            });
            return res;
        };
    
        auto ParamConstructor=batchParam(m_Constructor.GetParam());
        stack.insert(stack.end(),ParamConstructor.begin(),ParamConstructor.end());
        
        auto ParamAddFTime=batchParam(m_AddFTime.GetParam());
        stack.insert(stack.end(),ParamAddFTime.begin(),ParamAddFTime.end());
    
        auto ParamLabel=batchParam(m_GetLabel.GetParam());
        stack.insert(stack.end(),ParamLabel.begin(),ParamLabel.end());
        nvtxRangePop();
        nvtxRangePop();
        return DataEval{elem,stack};
    };
    
    /**
     * @brief Constructor of the computational graph.
     */
    RandomizeRnnDoubleEval(std::vector<size_t> nn_hidden):RandomizeRnnDouble(nn_hidden)
    {
        std::function<Cloud(Cloud,pcl::PointXYZ)> fAddFTime=[](Cloud cloud,pcl::PointXYZ point)
        {
            return cloud;
        };
        m_AddFTime=m_BuilderGRU.Build(fAddFTime);
        
        std::function<Cloud()> fConstructor=[]()
        {
            return Cloud();
        };
        m_Constructor=m_Builder.Build(fConstructor);
        
        std::function<Label(Cloud)> fGetLabel=[](Cloud)
        {
            return Label();
        };
        m_GetLabel=m_Builder.Build(fGetLabel);
        
        torch::jit::Stack param;
        
        auto AddFTimeParam=m_AddFTime.GetParam();
        param.insert(param.end(),AddFTimeParam.begin(),AddFTimeParam.end());
        
        auto ConstructorParam=m_Constructor.GetParam();
        param.insert(param.end(),ConstructorParam.begin(),ConstructorParam.end());
        
        auto GetLabelParam=m_GetLabel.GetParam();
        param.insert(param.end(),GetLabelParam.begin(),GetLabelParam.end());
       
        std::transform(param.begin(),param.end(),std::back_inserter(m_paramTensor),[](auto elem)
        {
            return elem.toTensor();
        });
        
        auto graph=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable Values = torch::jit::SymbolicVariable::asNewInput(*graph);
        torch::jit::SymbolicVariable Size = torch::jit::SymbolicVariable::asNewInput(*graph);
        torch::jit::SymbolicVariable Label = torch::jit::SymbolicVariable::asNewInput(*graph);
        
        auto paramConstructor=m_Constructor.CreateSymbolicParameters(graph);
        
        auto paramAddFTime=m_AddFTime.CreateSymbolicParameters(graph);
        
        auto paramGetLabel=m_GetLabel.CreateSymbolicParameters(graph);
        
        torch::jit::SymbolicVariable stateEmpty=m_Constructor.InsertNode(graph,paramConstructor,{})[0];

        auto oneConstant=graph->insertConstant(1);
        auto falseConstant=graph->insertConstant(false);
        auto negoneConstant=graph->insertConstant(-1);
        auto zeroConstant=graph->insertConstant(at::zeros({},at::kCUDA));
        auto cond=Size>torch::jit::SymbolicVariable(negoneConstant);
        
        auto MaxLoop=graph->insertConstant(at::ones({},at::kCUDA)*10000);
        
        auto loop=graph->insertNode(graph->create(torch::jit::prim::Loop,1));
        loop->addInput(MaxLoop);
        loop->addInput(cond);
        loop->addInput(stateEmpty);
        {
            auto body=loop->addBlock();
            auto counter=body->insertInput(0);
            auto state=body->insertInput(1);
            torch::jit::WithInsertPoint guard(body);
            
            auto select=body->appendNode(graph->create(torch::jit::aten::select,1));
            select->addInput(Values);
            select->addInput(oneConstant);
            select->addInput(counter);
            auto output_select=torch::jit::SymbolicVariable(select->outputs()[0]);
        
            auto new_state=m_AddFTime.InsertNode(body->owningGraph(),paramAddFTime,{state,output_select})[0];
            
            auto cond=Size>torch::jit::SymbolicVariable(counter);
            body->registerOutput(cond);
            body->registerOutput(new_state);
        }
          
        auto output_state=loop->outputs()[0];
        
        auto PredAct=m_GetLabel.InsertNode(graph,paramGetLabel,{output_state})[0];
        auto PredNode=graph->appendNode(graph->create(torch::jit::aten::softmax,1));
        PredNode->addInput(PredAct);
        PredNode->addInput(oneConstant);
        graph->registerOutput(PredNode->output());
        graph->lint();
        auto graphBatch=to_batch_graph(graph);
        auto graphFinal=std::make_shared<torch::jit::Graph>();
        
        for(int i=0;i<graphBatch->inputs().size();++i)
        {
            graphFinal->addInput(graphBatch->inputs()[i]->uniqueName());
        }
        auto results=torch::jit::inlineCallTo(*graphFinal,*graphBatch,graphFinal->inputs(),true);
        auto resultData=results[0];
        auto resultMask=results[1];
        auto afterMask=static_cast<torch::jit::SymbolicVariable>(resultData)*static_cast<torch::jit::SymbolicVariable>(resultMask).type_as(resultData);
        torch::jit::SymbolicVariable(afterMask).addAsOutput();
        graphFinal->lint();
        m_executor=torch::jit::GraphExecutor(graphFinal,true);
    };
    auto Execute(torch::jit::Stack stack)
    {
        RunReturn ret;
        m_executor.run(stack);
        return stack[0].toTensor().sum(0,false);
    };
};
