#pragma once
#include "Core/Experiment.h"
#include <deque>
#include "ExperimentDataPointCloud.h"

class ExperimentDoubleRnnExtracted : public Experiment<ExperimentDataPointCloud>
{
    int m_nnLayer;
    int m_nnHidden;
    int m_lstmHidden;
    int m_sample;
    int m_extractedLayer;
    int m_extractedHidden;
    double m_learningRate;
    bool m_curiculum;
    bool m_regularizer;
    
public:
    ExperimentDoubleRnnExtracted(int nnLayer,int nnHidden,int lstmHidden,int sample,std::string name, std::string nameData,size_t nbVal,double learningRate,bool curiculum,bool regularizer,int layerExtracted,int hiddenExtracted): Experiment<ExperimentDataPointCloud>(name,nameData,nbVal),m_nnLayer(nnLayer),m_nnHidden(nnHidden),m_lstmHidden(lstmHidden),m_sample(sample),m_learningRate(learningRate),m_curiculum(curiculum),m_regularizer(regularizer),m_extractedLayer(layerExtracted),m_extractedHidden(hiddenExtracted)
    {
    };
    
    void PrintDescription() const
    {
        Experiment<ExperimentDataPointCloud>::PrintDescription();
        std::cout<<"nnLayer: "<<m_nnLayer<<std::endl;
        std::cout<<"nnHidden: "<<m_nnHidden<<std::endl;
        std::cout<<"lstmHidden: "<<m_lstmHidden<<std::endl;
        std::cout<<"sample: "<<m_sample<<std::endl;
        std::cout<<"LearningRate: "<<m_learningRate<<std::endl;
        std::cout<<"Curiculum: "<<m_curiculum<<std::endl;
        std::cout<<"layerExtracted: "<<m_extractedLayer<<std::endl;
        std::cout<<"hiddenExtracted: "<<m_extractedHidden<<std::endl;
    };
    std::string GetDirectoryPath() const
    {
        std::string nameParam=std::to_string(m_nnLayer)+std::string("_")+std::to_string(m_nnHidden)+std::string("_")+std::to_string(m_lstmHidden)+std::string("_")+std::to_string(m_sample)+std::string("_")+std::to_string(m_learningRate)+std::string("_")+std::to_string(m_extractedLayer)+std::string("_")+std::to_string(m_extractedHidden);
        if(m_curiculum)
        {
            nameParam+="_C";
        }
        if(m_regularizer)
        {
            nameParam+="_R";
        }
        return std::string("/Data/Result/PointCloud/RnnDoubleExtracted/")+GetName()+std::string("/")+GetDataName()+std::string("/")+nameParam;
    };
};


