#pragma once

#include "Core/ExperimentData.h"
#include <deque>
#include <string>
#include <boost/filesystem.hpp>
#include "PointCloud/Label.h"
#include "PointCloud/DataMesh.h"

class ExperimentDataPointCloud : public ExperimentData<std::deque<DataMesh>>
{
    size_t m_nbVal;
public:
    ExperimentDataPointCloud(std::string dataName,size_t nbVal): ExperimentData<std::deque<DataMesh>>(dataName),m_nbVal(nbVal)
    {
    }
    virtual std::string GetDirectoryPath() const
    {
        return std::string("/Data/Data/PointCloud/")+this->GetDataName();
    }
    virtual void Load(bool setPoly=true)
    {
        boost::filesystem::path p(GetDirectoryPath());
        int i=0;
        auto getData=[this,setPoly](auto file,auto label)
        {
            DataMesh data;
            data.label=label;
            data.file=file.path().native();
            if(setPoly)
            {
                data.SetPolyData();
            }
            return data;
        };
        
        for (boost::filesystem::directory_entry& labelDir : boost::filesystem::directory_iterator(p))
        {
            std::string name(labelDir.path().filename().native());
            Label::NameToId[name]=i;
            Label::IdToName[i]=name;
            Label label(i);
            ++i;
            std::cout<<"i "<<i<<" name "<<name<<std::endl;
            {
                boost::filesystem::path pathLabel=labelDir.path();
                pathLabel+=boost::filesystem::path("/train");
                int num=0;
                for(boost::filesystem::directory_entry& trainFile : boost::filesystem::directory_iterator(pathLabel))
                {
                    if(trainFile.path().extension()==".ply")
                    {
                        DataMesh data=getData(trainFile,label);
                        if(num<m_nbVal)
                        {
                            ValData().push_back(data);
                        }
                        else
                        {
                            TrainData().push_back(data);
                        }
                        ++num;
                    }
                }
            }
            {
                boost::filesystem::path pathLabel=labelDir.path();
                pathLabel+=boost::filesystem::path("/test");
                int num=0;
                for(boost::filesystem::directory_entry& testFile : boost::filesystem::directory_iterator(pathLabel))
                {
                    if(testFile.path().extension()==".ply")
                    {
                        DataMesh data=getData(testFile,label);
                        TestData().push_back(data);
                        ++num;
                    }
                }
            }
        }
        Label::size=i;
    };
    using ExperimentData<std::deque<DataMesh>>::type_data;
};

