#pragma once
#include "Core/FunctionOp.h"
#include "PointCloud/Point.h"
#include "PointCloud/HiddenPoint.h"
#include "PointCloud/Label.h"
#include "PointCloud/Cloud.h"
#include "torch/csrc/jit/batched/BatchTensor.h"
#include "Builder/MLBuilder.h"
#include "Builder/MLBuilderMLP.h"
#include "Builder/MLBuilderGRU.h"
#include "Core/Sample.h"
#include <nvToolsExt.h>

/**
 * @brief Class that create the computation graph to Solve the Orbiwise problem.
 */
class RandomizeRnnDoubleExtracted
{
    protected:
    /**
     * @brief Builder for normal neural network.
     */
    MLBuilderMLP<> m_Builder;
    /**
     * @brief Builder for GRU 
     */
    MLBuilderGRU<> m_BuilderGRU;
    /**
     * @brief AddSet
     */
    FunctionOp<std::function<ML<Cloud>(ML<Cloud>,ML<HiddenPoint>)>> m_AddFTime;
    /**
     * @brief Function to create initial state.
     */
    FunctionOp<std::function<ML<Cloud>()>> m_Constructor;
    /**
     * @brief Function classify.
     */
    FunctionOp<std::function<ML<Label>(ML<Cloud>)>> m_GetLabel;
    
    /**
     * @brief Function extract feature.
     */
    FunctionOp<std::function<ML<HiddenPoint>(ML<pcl::PointXYZ>)>> m_extract;
    /**
     * @brief Storage of the graph Executor;
     */
    torch::jit::GraphExecutor m_executor;
    /**
     * @brief vector of Parameter.
     * */
    std::vector<at::Tensor> m_paramTensor;
    /**
     * @brief List of Tensors containing information about one instance.
     */
    struct TensorsLists
    {
        at::Tensor Value;
        at::Tensor Size;
        at::Tensor Label;
        at::Tensor Value2;
        at::Tensor Same;
    };
    
    /**
     * @brief Structure containing value returned.
     */
    struct RunReturn
    {
        torch::autograd::Variable sum;
        torch::autograd::Variable sumNotNorm;
        torch::autograd::Variable acc;
    };
    
    /**
     * @brief structure used to return value for the evaluation.
     */
    struct PredReturn
    {
        double Pred;
        double Target;
    };
    
    /**
     * @brief Function that create a single tensor.
     */
    TensorsLists SingleToTensor(const Cloud& cloud)
    {
        TensorsLists ret;
        auto points=cloud.GetPoints();
        size_t size=points->size();
        ret.Size=at::empty({1,1},at::CUDA(at::kInt));
        ret.Size[0][0]=int(size-1);
        ret.Value=at::empty({1,size,6}, at::kFloat);
        auto Accessor=ret.Value.packed_accessor<float,3>();
        for(size_t i=0;i<size;++i)
        {
            auto tensor=Accessor[0][i];
            m_Builder.GetML((*points)[i],tensor);
        }
        ret.Value=ret.Value.cuda();
        auto oneHot=at::zeros({size,size},at::kCUDA);
        auto oneHot2=at::zeros({size,size},at::kCUDA);
        auto permutation=at::randperm(size,at::CUDA(at::kLong));
        permutation=permutation.view({1,size});
        auto permutation2=at::randperm(size,at::CUDA(at::kLong));
        permutation2=permutation2.view({1,size});
        oneHot.scatter_(0,permutation,1);
        oneHot2.scatter_(0,permutation2,1);
        auto cumOneHot=oneHot.cumsum(1);
        auto cumOneHot2=oneHot2.cumsum(1);
            
        auto equalTensor=cumOneHot==cumOneHot2;
        auto boolCond=at::all(equalTensor,0);
        ret.Same=boolCond.view({1,size});

        permutation=permutation.view({size});
        ret.Value=at::index_select(ret.Value,1,permutation);
        permutation2=permutation2.view({size});
        ret.Value2=at::index_select(ret.Value,1,permutation2);
        ret.Label=at::zeros({1,Label::size},at::kCUDA);
        m_Builder.GetML(cloud.GetLabel(),ret.Label[0]);
        return ret;
    };
    
public:
    /**
     * @brief Get the param Tensor.
     */
    std::vector<at::Tensor>& GetTensor()
    {
        return m_paramTensor;
    };
    
    /**
     * @brief Transform to Batch Tensor.
     */
    template <typename Iterator>
    torch::jit::Stack ToBatchTensor(Iterator begin,Iterator end,size_t sample)
    {
        nvtxRangePushA("BatchToTensor");
        std::vector<at::Tensor> Value;
        std::vector<at::Tensor> Value2;
        std::vector<at::Tensor> Size;
        std::vector<at::Tensor> Label;
        std::vector<at::Tensor> Same;
        int nb=0;
        static size_t nbiter=0;
        ++nbiter;
        nvtxRangePushA("Generate");
        std::for_each(begin,end,[&](auto& elem)
        {
            elem.nbSample=sample;
            nvtxRangePushA("Sample");
            auto cloud=elem.Sample();
            nvtxRangePop();
            ++nb;
            nvtxRangePushA("CreateTensors");
            auto [tenValue,tenSize,tenLabel,tenValue2,tenSame]=SingleToTensor(cloud);
            nvtxRangePop();
            Value.push_back(tenValue);
            Value2.push_back(tenValue2);
            Size.push_back(tenSize);
            Label.push_back(tenLabel);
            Same.push_back(tenSame);
        }
        );
        nvtxRangePop();
        nvtxRangePushA("CreateStack");
        auto StaticDim=at::ones({1},at::kByte);
        StaticDim[0]=false;
        auto DynamicDim=at::ones({2},at::kByte);
        //DynamicDim[0]=false;
        DynamicDim[0]=true;
        DynamicDim[1]=false;
        
        auto SameDim=at::ones({1},at::kByte);
        SameDim[0]=true;
        
        torch::jit::BatchTensor BatchValue(Value,DynamicDim);
        torch::jit::BatchTensor BatchValue2(Value2,DynamicDim);
        torch::jit::BatchTensor BatchSize(Size,StaticDim);
        torch::jit::BatchTensor BatchLabel(Label,StaticDim);
        torch::jit::BatchTensor BatchSame(Same,SameDim);
        torch::jit::BatchTensor BatchError(at::zeros({},at::kCUDA),nb);
        
        auto funcVar=[](auto elem)
        {
            return torch::autograd::make_variable(elem.to(at::Device("cuda")),false);
        };
        
        torch::jit::Stack stack({funcVar(BatchValue.get_data()),funcVar(BatchValue.get_mask()),funcVar(BatchValue.get_dims()),funcVar(BatchValue2.get_data()),funcVar(BatchValue2.get_mask()),funcVar(BatchValue2.get_dims()),funcVar(BatchSize.get_data()),funcVar(BatchSize.get_mask()),funcVar(BatchSize.get_dims()),funcVar(BatchLabel.get_data()),funcVar(BatchLabel.get_mask()),funcVar(BatchLabel.get_dims()),funcVar(BatchSame.get_data()),funcVar(BatchSame.get_mask()),funcVar(BatchSame.get_dims()),funcVar(BatchError.get_data()),funcVar(BatchError.get_mask()),funcVar(BatchError.get_dims())});
                
        auto batchParam=[](auto elem)
        {
            torch::jit::Stack res;
            std::for_each(elem.begin(),elem.end(),[&res](auto elem2)
            {
                auto var=elem2.toTensor();
                var.set_requires_grad(true);
                torch::jit::BatchTensor batch(var,size_t(1));
                res.push_back(batch.get_data());
                res.push_back(batch.get_mask());
                res.push_back(batch.get_dims());
            });
            return res;
        };
    
        auto ParamConstructor=batchParam(m_Constructor.GetParam());
        stack.insert(stack.end(),ParamConstructor.begin(),ParamConstructor.end());
        
        auto ParamAddFTime=batchParam(m_AddFTime.GetParam());
        stack.insert(stack.end(),ParamAddFTime.begin(),ParamAddFTime.end());
    
        auto ParamLabel=batchParam(m_GetLabel.GetParam());
        stack.insert(stack.end(),ParamLabel.begin(),ParamLabel.end());
        
        auto ParamExtract=batchParam(m_extract.GetParam());
        stack.insert(stack.end(),ParamExtract.begin(),ParamExtract.end());
        nvtxRangePop();
        nvtxRangePop();
        return stack;
    };
    
    /**
     * @brief Constructor of the computational graph.
     */
    RandomizeRnnDoubleExtracted(std::vector<size_t> nn_hidden,std::vector<size_t> extract_hidden):m_Builder(at::CUDA(at::kFloat),nn_hidden),m_BuilderGRU(at::CUDA(at::kFloat))
    {
        std::function<Cloud(Cloud,HiddenPoint)> fAddFTime=[](Cloud cloud,HiddenPoint point)
        {
            return cloud;
        };
        m_AddFTime=m_BuilderGRU.Build(fAddFTime);
        
        std::function<Cloud()> fConstructor=[]()
        {
            return Cloud();
        };
        m_Constructor=m_Builder.Build(fConstructor);
        
        std::function<Label(Cloud)> fGetLabel=[](Cloud)
        {
            return Label();
        };
        m_GetLabel=m_Builder.Build(fGetLabel);
        
        std::function<HiddenPoint(pcl::PointXYZ)> fExtract=[](pcl::PointXYZ)
        {
            return HiddenPoint();
        };
        m_extract=m_Builder.Build(fExtract);
        
        torch::jit::Stack param;
        
        auto AddFTimeParam=m_AddFTime.GetParam();
        param.insert(param.end(),AddFTimeParam.begin(),AddFTimeParam.end());
        
        auto ConstructorParam=m_Constructor.GetParam();
        param.insert(param.end(),ConstructorParam.begin(),ConstructorParam.end());
        
        auto GetLabelParam=m_GetLabel.GetParam();
        param.insert(param.end(),GetLabelParam.begin(),GetLabelParam.end());
        
        auto ExtractParam=m_extract.GetParam();
        param.insert(param.end(),ExtractParam.begin(),ExtractParam.end());
       
        std::transform(param.begin(),param.end(),std::back_inserter(m_paramTensor),[](auto elem)
        {
            return elem.toTensor();
        });
        
        auto graph=std::make_shared<torch::jit::Graph>();
        torch::jit::SymbolicVariable Values = torch::jit::SymbolicVariable::asNewInput(*graph);
        torch::jit::SymbolicVariable Values2 = torch::jit::SymbolicVariable::asNewInput(*graph);
        torch::jit::SymbolicVariable Size = torch::jit::SymbolicVariable::asNewInput(*graph);
        torch::jit::SymbolicVariable Label = torch::jit::SymbolicVariable::asNewInput(*graph);
        torch::jit::SymbolicVariable Same = torch::jit::SymbolicVariable::asNewInput(*graph);
        torch::jit::SymbolicVariable InitialError = torch::jit::SymbolicVariable::asNewInput(*graph);
        
        auto paramConstructor=m_Constructor.CreateSymbolicParameters(graph);
        
        auto paramAddFTime=m_AddFTime.CreateSymbolicParameters(graph);
        
        auto paramGetLabel=m_GetLabel.CreateSymbolicParameters(graph);
        
        auto paramExtract=m_extract.CreateSymbolicParameters(graph);
        
        torch::jit::SymbolicVariable stateEmpty=m_Constructor.InsertNode(graph,paramConstructor,{})[0];

        auto oneConstant=graph->insertConstant(1);
        auto falseConstant=graph->insertConstant(false);
        auto negoneConstant=graph->insertConstant(-1);
        auto zeroConstant=graph->insertConstant(at::zeros({},at::kCUDA));
        auto cond=Size>torch::jit::SymbolicVariable(negoneConstant);
        
        auto MaxLoop=graph->insertConstant(at::ones({},at::kCUDA)*10000);
        
        auto loop=graph->insertNode(graph->create(torch::jit::prim::Loop,3));
        loop->addInput(MaxLoop);
        loop->addInput(cond);
        loop->addInput(stateEmpty);
        loop->addInput(stateEmpty);
        loop->addInput(InitialError);
        {
            auto body=loop->addBlock();
            auto counter=body->insertInput(0);
            auto state=body->insertInput(1);
            auto state2=body->insertInput(2);
            auto regError=body->insertInput(3);
            torch::jit::WithInsertPoint guard(body);
            
            auto select=body->appendNode(graph->create(torch::jit::aten::select,1));
            select->addInput(Values);
            select->addInput(oneConstant);
            select->addInput(counter);
            auto output_select=torch::jit::SymbolicVariable(select->outputs()[0]);
            
            auto output_extract=m_extract.InsertNode(body->owningGraph(),paramExtract,{output_select})[0];
        
            auto new_state=m_AddFTime.InsertNode(body->owningGraph(),paramAddFTime,{state,output_extract})[0];
            
            auto select2=body->appendNode(graph->create(torch::jit::aten::select,1));
            select2->addInput(Values2);
            select2->addInput(oneConstant);
            select2->addInput(counter);
            auto output_select2=torch::jit::SymbolicVariable(select2->outputs()[0]);
            
            auto output_extract2=m_extract.InsertNode(body->owningGraph(),paramExtract,{output_select2})[0];
        
            auto new_state2=m_AddFTime.InsertNode(body->owningGraph(),paramAddFTime,{state2,output_extract2})[0];
            
            auto same_node=body->appendNode(graph->create(torch::jit::aten::select,1));
            same_node->addInput(Same);
            same_node->addInput(oneConstant);
            same_node->addInput(counter);
            auto output_same=torch::jit::SymbolicVariable(same_node->outputs()[0]);
            
            auto errorState=(new_state-new_state2)*(new_state-new_state2);
            auto errorSummed=errorState.sum();
            
            auto zeroError=torch::jit::SymbolicVariable::zeros_like(errorSummed);
            
            auto same_error=body->appendNode(graph->create(torch::jit::aten::where,1));
            same_error->addInput(output_same);
            same_error->addInput(errorSummed);
            same_error->addInput(zeroError);
            auto output_same_error=torch::jit::SymbolicVariable(same_error->output());
            auto sum_error=output_same_error.sum();

            auto new_error=torch::jit::SymbolicVariable(regError)+sum_error;

            
            auto cond=Size>torch::jit::SymbolicVariable(counter);
            body->registerOutput(cond);
            body->registerOutput(new_state);
            body->registerOutput(new_state2);
            body->registerOutput(new_error);
        }
          
        auto output_state=loop->outputs()[0];
        
        auto output_state2=loop->outputs()[1];
        
        auto output_regularizer=loop->outputs()[2];
        
        auto PredAct=m_GetLabel.InsertNode(graph,paramGetLabel,{output_state})[0];
        
        auto PredAct2=m_GetLabel.InsertNode(graph,paramGetLabel,{output_state2})[0];
        
        auto PredNode=graph->appendNode(graph->create(torch::jit::aten::log_softmax,1));
        PredNode->addInput(PredAct);
        PredNode->addInput(oneConstant);
        
        auto PredNode2=graph->appendNode(graph->create(torch::jit::aten::log_softmax,1));
        PredNode2->addInput(PredAct2);
        PredNode2->addInput(oneConstant);
        
        auto Pred=torch::jit::SymbolicVariable(PredNode->output());
        auto Pred2=torch::jit::SymbolicVariable(PredNode2->output());
        auto error=-Pred*Label-Pred2*Label;
        auto errorSum=error.sum()*0.5;
        
        auto error2Node=graph->appendNode(graph->create(torch::jit::aten::argmax,1));
        error2Node->addInput(PredAct);
        error2Node->addInput(oneConstant);
        error2Node->addInput(falseConstant);
        auto error2Output=torch::jit::SymbolicVariable(error2Node->output());
        auto error3Node=graph->appendNode(graph->create(torch::jit::aten::index_select,1));
        error3Node->addInput(Label);
        error3Node->addInput(oneConstant);
        error3Node->addInput(error2Output);
        auto error3=torch::jit::SymbolicVariable(error3Node->output());
        
        graph->registerOutput(errorSum);
        graph->registerOutput(error3);
        graph->lint();
        auto graphBatch=to_batch_graph(graph);
        auto graphFinal=std::make_shared<torch::jit::Graph>();
        
        for(int i=0;i<graphBatch->inputs().size();++i)
        {
            graphFinal->addInput(graphBatch->inputs()[i]->uniqueName());
        }
        auto results=torch::jit::inlineCallTo(*graphFinal,*graphBatch,graphFinal->inputs(),true);
        auto resultData=results[0];
        auto resultMask=results[1];
        auto resultDim=results[2];
        auto result2Data=results[3];
        auto result2Mask=results[4];
        auto result2Dim=results[5];
        auto afterMask=static_cast<torch::jit::SymbolicVariable>(resultData)*static_cast<torch::jit::SymbolicVariable>(resultMask).type_as(resultData);
        auto afterMask2=static_cast<torch::jit::SymbolicVariable>(result2Data)*static_cast<torch::jit::SymbolicVariable>(result2Mask).type_as(result2Data);
        auto oneConstantFinal=graphFinal->insertConstant(0);
        auto size_Node=graphFinal->insertNode(graphFinal->create(torch::jit::aten::size,1));
        size_Node->addInput(afterMask);
        size_Node->addInput(oneConstantFinal);
        auto final_Size=size_Node->outputs()[0];
        auto valSum=static_cast<torch::jit::SymbolicVariable>(afterMask).sum();
        auto accSum=static_cast<torch::jit::SymbolicVariable>(afterMask2).sum();
        torch::jit::SymbolicVariable(valSum).addAsOutput();
        torch::jit::SymbolicVariable(final_Size).addAsOutput();
        torch::jit::SymbolicVariable(accSum).addAsOutput();
        graphFinal->lint();
        //graphFinal->dump();
        /*std::for_each(torch::jit::ToBatch::batch_operator_table.begin(),torch::jit::ToBatch::batch_operator_table.end(),[](auto elem)
        {
            std::cout<<elem.first<<std::endl;
            std::for_each(elem.second.begin(),elem.second.end(),[](auto elem2)
            {
                elem2->dump();
            });
        });*/
        m_executor=torch::jit::GraphExecutor(graphFinal,true);
    };
    RunReturn Execute(torch::jit::Stack stack)
    {
        RunReturn ret;
        m_executor.run(stack);
        int nb=stack[1].toInt();
        ret.sum=stack[0].toTensor()/nb;
        ret.sumNotNorm=stack[0].toTensor();
        ret.acc=stack[2].toTensor();
        return ret;
    };

    /*PredReturn Predict(const Message& m)
    {
        auto fAddFTime=m_AddFTime.GetFunction();
        auto fGetLat=m_GetLat.GetFunction();
        auto fGetLong=m_GetLong.GetFunction();
        auto setFTime=m.GetSetFTime();
        ML<Message> state;
        if(m_useTril)
        {
            auto fConstructor=m_ConstructorTril.value().GetFunction();
            state=fConstructor(m_Builder.GetML(m.GetTrilLatitude()),m_Builder.GetML(m.GetTrilLongitude()));
        }
        else
        {
            auto fConstructor=m_Constructor.value().GetFunction();
            state=fConstructor();
        }
        for(auto elem : setFTime)
        {
            state=fAddFTime(state,m_Builder.GetML(elem),m_Builder.GetML(m.GetFTime(elem)/m_timeScale));
        }
        double Lat=static_cast<double>(fGetLat(state));
        double Long=static_cast<double>(fGetLong(state));
        PredReturn ret;
        ret.PredLong=Long;
        ret.PredLat=Lat;
        ret.TargetLong=m.GetLongitude();
        ret.TargetLat=m.GetLatitude();
        ret.TrilLong=m.GetTrilLongitude();
        ret.TrilLat=m.GetTrilLatitude();
        return ret;
    }*/
    
    
    /**
     * @brief Load member function which update the parameters with a version loaded from the archive.
     * @param archive Archive were the parameters are stored.
     */
    void Load(torch::serialize::InputArchive& archive)
    {
        auto loadElem=[&archive](std::string Name,auto& functionOp)
        {
            torch::serialize::InputArchive archiveTemp;
            archive.read(Name,archiveTemp);
            functionOp.Load(archiveTemp);
        };
        loadElem("Constructor",m_Constructor);
        loadElem("AddFTime",m_AddFTime);
        loadElem("GetLabel",m_GetLabel);
        loadElem("Extract",m_extract);
    }
    /**
     * @brief Save member function which save the current parameter and return and archive that can be used to save the content to disc.
     */
    torch::serialize::OutputArchive Save() const
    {
        torch::serialize::OutputArchive output;
        auto saveElem=[&output](std::string Name,auto& functionOp)
        {
            auto archive=functionOp.Save();
            output.write(Name,archive);
        };
        saveElem("Constructor",m_Constructor);
        saveElem("AddFTime",m_AddFTime);
        saveElem("GetLabel",m_GetLabel);
        saveElem("Extract",m_extract);
        return output;
    }
};


