#pragma once
#include "Core/Experiment.h"
#include <deque>
#include "ExperimentDataPointCloud.h"

class ExperimentRandomizeRnn : public Experiment<ExperimentDataPointCloud>
{
    int m_nnLayer;
    int m_nnHidden;
    int m_lstmHidden;
    int m_sample;
    bool m_randomize;
    
public:
    ExperimentRandomizeRnn(int nnLayer,int nnHidden,int lstmHidden,bool randomize,int sample,std::string name, std::string nameData,size_t nbVal): Experiment<ExperimentDataPointCloud>(name,nameData,nbVal),m_nnLayer(nnLayer),m_nnHidden(nnHidden),m_lstmHidden(lstmHidden),m_randomize(randomize),m_sample(sample)
    {
    };
    
    void PrintDescription() const
    {
        Experiment<ExperimentDataPointCloud>::PrintDescription();
        std::cout<<"nnLayer: "<<m_nnLayer<<std::endl;
        std::cout<<"nnHidden: "<<m_nnHidden<<std::endl;
        std::cout<<"lstmHidden: "<<m_lstmHidden<<std::endl;
        std::cout<<"randomize: "<<m_randomize<<std::endl;
        std::cout<<"sample: "<<m_sample<<std::endl;
    };
    std::string GetDirectoryPath() const
    {
        std::string nameParam=std::to_string(m_nnLayer)+std::string("_")+std::to_string(m_nnHidden)+std::string("_")+std::to_string(m_lstmHidden)+std::string("_")+std::to_string(m_sample)+std::string("_")+std::to_string(m_randomize);
        return std::string("/Data/Result/PointCloud/RandomizeRnn/")+GetName()+std::string("/")+GetDataName()+std::string("/")+nameParam;
    };
};


