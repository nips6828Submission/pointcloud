#include "Core/StartUp.h"
StartUp start;
#include <iostream>
#include <gtest/gtest.h>
#include "API/Points.h"
#include "Core/ExpressionGenerator.h"
#include "Builder/MLBuilderGRU.h"
#include "Core/ComputationGraphGenerator.h"
#include <random>
#include "Loss/LeastSquare.h"

#include "torch/csrc/autograd/variable.h"
#include "torch/csrc/jit/symbolic_variable.h"
#include "torch/csrc/jit/autodiff.h"
#include "torch/csrc/jit/graph_executor.h"
#include "torch/csrc/autograd/function.h"
#include "torch/csrc/jit/passes/to_batch.h"
#include "torch/csrc/jit/batched/BatchTensor.h"
#include "torch/csrc/jit/custom_operator.h"
#include "Core/ExpressionGenerator.h"
#include "Core/GenerateMLValue.h"
#include "Core/ComputationGraphGenerator.h"

#include "Core/Backward.h"
#include <functional>
#include "Core/FunctionOp.h"

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <fstream>
#include <boost/serialization/vector.hpp>
#include "Serialization/Tensor.h"

#include "Core/Symbolic_Variable.h"
#include "Core/Expression.h"
#include "Builder/MLBuilder.h"
#include "Builder/MLBuilderMLP.h"
#include "Builder/MLBuilderMLPSigmoid.h"
#include "Loss/LeastSquare.h"

#include <random>

#include <future>


TEST(PointsAPI,TestExpressionGenerator)
{
    try
    {
        MLBuilderGRU builderGRU;
        MLBuilderMLP builderMLP;
        typedef std::mt19937_64 type_generator;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937_64 generator(seed);
        ComputationGraphGenerator<ExpressionGenerator<std::mt19937_64>> Graph(5);
        Graph.GetGenerator().SetMaxDepth(2);
        std::function<PointId(type_generator&,size_t)> pointGenerator=[](type_generator& g,size_t depth)
        {
            std::uniform_int_distribution<> distribution(0,2);
            return PointId(distribution(g));
        };
        Graph.InsertGenerator(pointGenerator,true);
        
        std::function<Point3d(type_generator&,size_t)> point3dGenerrator=[](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1,1);
            Point3d Point{distribution(g),distribution(g),distribution(g)};
            return Point;
        };
        Graph.InsertGenerator(point3dGenerrator,true);
        
        std::function<Point3d(Points points,PointId number)> func_Points_GetPoint(Points_GetPoint);
        auto myPoints_GetPoint=builderMLP.Build(func_Points_GetPoint);
        Graph.Insert(myPoints_GetPoint,Points_GetPointGen<type_generator,Expression>(),true);
        
        std::function<Points(Points points,Point3d point)> func_Points_InsertNextPoint(Points_InsertNextPoint);
        auto myPoints_InsertNextPoint=builderMLP.Build(func_Points_InsertNextPoint);
        Graph.Insert(myPoints_InsertNextPoint);
        
        std::function<Points()> func_PointsConstructor(PointsConstructor);
        auto myPointsConstructor=builderMLP.Build(func_PointsConstructor);
        Graph.Insert(myPointsConstructor,true);
        
        std::function<double(Point3d,Point3d)> funcLoss;
        auto loss=LeastSquareLoss(funcLoss);
        Graph.InsertLoss(loss);
        
        Graph.CreateGraphExecutor();
        
        auto stack=Graph.GenerateStack<Point3d>(1,generator);
        auto error=Graph.run<Point3d>(stack.first);
        std::cout<<"error "<<error<<std::endl;
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}

struct HiddenPoint3d
{
    Point3d point;
};

template <>
class ML<HiddenPoint3d>: public at::Tensor
{
    public:
    static size_t m_size;
    static size_t size()
    {
        return m_size;
    }
    ML()
    {
    }
    ML(at::Tensor embedded):at::Tensor(embedded)
    {
        
    }
    static constexpr bool IsFloatCompatible()
    {
        return false;
    }
    typedef HiddenPoint3d Type;
};

size_t ML<HiddenPoint3d>::m_size=64;

TEST(PointsAPI,TestExpressionGenerator2)
{
    try
    {
        std::chrono::steady_clock clock;
        typedef std::chrono::duration<double> duration_t;
        std::vector<size_t> hidden_Form(1,256);
        MLBuilderMLPSigmoid builderSigmoid(at::CUDA(at::kFloat),hidden_Form);
        MLBuilderMLP builderMLP(at::CUDA(at::kFloat),hidden_Form);
        MLBuilderMLP builderReconstruct(at::CUDA(at::kFloat),hidden_Form);
        typedef std::mt19937_64 type_generator;
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937_64 generator(seed);
        ComputationGraphGenerator<ExpressionGenerator<std::mt19937_64>> Graph(200);
        Graph.GetGenerator().SetMaxDepth(10);
        std::function<PointId(type_generator&,size_t)> pointGenerator=[](type_generator& g,size_t depth)
        {
            std::uniform_int_distribution<> distribution(0,20);
            return distribution(g);
        };
        Graph.InsertGenerator(pointGenerator,true);
        
        std::function<Point3d(type_generator&,size_t)> point3dGenerrator=[](type_generator& g,size_t depth)
        {
            std::uniform_real_distribution<double> distribution(-1,1);
            Point3d Point{distribution(g),distribution(g),distribution(g)};
            return Point;
        };
        Graph.InsertGenerator(point3dGenerrator,true);
        
        std::function<HiddenPoint3d(Points points,PointId number)> func_Points_GetPoint=[](Points points,PointId number)
        {
            return HiddenPoint3d{Points_GetPoint(points,number)};
        };
        auto myPoints_GetPoint=builderMLP.Build(func_Points_GetPoint);
        Graph.Insert(myPoints_GetPoint,Points_GetPointGen<type_generator,Expression>(),true);
        
        std::function<Points(Points points,HiddenPoint3d point)> func_Points_InsertNextPoint=[](Points points,HiddenPoint3d point)
        {
            return Points_InsertNextPoint(points,point.point);
        };
        auto myPoints_InsertNextPoint=builderMLP.Build(func_Points_InsertNextPoint);
        Graph.Insert(myPoints_InsertNextPoint);
        
        std::function<HiddenPoint3d(Point3d)> ToHidden=[](Point3d point)
        {
            return HiddenPoint3d{point};
        };
        auto myToHidden=builderMLP.Build(ToHidden);
        Graph.Insert(myToHidden);
        
        std::function<Point3d(HiddenPoint3d)> FromHidden=[](HiddenPoint3d point)
        {
            return point.point;
        };
        auto myFromHidden=builderMLP.Build(FromHidden);
        Graph.Insert(myFromHidden,true);
        
        std::function<Points()> func_PointsConstructor(PointsConstructor);
        auto myPointsConstructor=builderMLP.Build(func_PointsConstructor);
        Graph.Insert(myPointsConstructor,true);
        
        std::function<double(Point3d,Point3d)> funcLoss;
        auto loss=LeastSquareLoss(funcLoss);
        Graph.InsertLoss(loss);
        Graph.CreateGraphExecutor();
        
        auto param=Graph.GetParam();
        auto adamOption=torch::optim::AdamOptions(1e-3);
        torch::optim::Adam adam(param,adamOption);
        torch::optim::Adam adamSlow(param,adamOption);
        
        auto funcFill=[&Graph,&generator](int nbInstance)
        {
            auto res=Graph.GenerateStack<Point3d>(nbInstance,generator);
            return res;
        };
        typedef decltype(std::async(std::launch::async,funcFill,10)) TypeFuture;
        std::queue<TypeFuture> queue;
        std::queue<TypeFuture> queueSlow;
        auto FillQueue=[&]()
        {
            Graph.GetGenerator().SetMaxDepth(10);
            for(size_t batch=0;batch<20;++batch)
            {
                auto input=std::async(std::launch::async,funcFill,10);
                queueSlow.push(std::move(input));
            }
            Graph.GetGenerator().SetMaxDepth(10);
            for(size_t batch=0;batch<20;++batch)
            {
                auto input=std::async(std::launch::async,funcFill,5);
                queue.push(std::move(input));
            }
        };
        FillQueue();
        double error=1000;
        for(int i=0;i<100;++i)
        {
            double weightNorm=0;
            std::queue<TypeFuture>* whichQueue;
            torch::optim::Adam* whichAdam;
            if(i%2==0)
            {
                Graph.GetGenerator().SetMaxDepth(10);
                whichQueue=&queueSlow;
                whichAdam=&adamSlow;
            }
            else
            {
                Graph.GetGenerator().SetMaxDepth(5);
                whichQueue=&queue;
                whichAdam=&adam;
            }
            int nbInstance=10;
            whichAdam->zero_grad();
            torch::autograd::Variable one=torch::autograd::make_variable(at::ones({1},at::kCUDA),false);
            auto start_time = clock.now();
            auto stack=whichQueue->front().get();
            auto copyStack=stack;
            duration_t difftime=clock.now()-start_time;
            std::cout<<"wait time "<<difftime.count()<<std::endl;
            whichQueue->pop();
            auto inputnew=std::async(std::launch::async,funcFill,nbInstance);
            whichQueue->push(std::move(inputnew));
            auto start_timeRun = clock.now();
            auto errorvar=Graph.run<Point3d>(stack.first);
            Backward(errorvar,one);
            duration_t difftimeRun=clock.now()-start_timeRun;
            std::cout<<"Run time "<<difftimeRun.count()<<std::endl;
            std::cout<<"Run time Per instance"<<difftimeRun.count()/nbInstance<<std::endl;
            std::for_each(param.begin(),param.end(),[&weightNorm](auto elem)
            {
                if(elem.grad().defined())
                {
                    //elem.grad().detach_();
                    //elem.grad().clamp_(-5,5);
                    torch::autograd::Variable normWeight=elem.grad().norm();
                    weightNorm=std::max(normWeight.item<double>(),weightNorm);
                }
            });
            bool nanProblem=false;
            std::for_each(param.begin(),param.end(),[&nanProblem](auto elem)
            {
                if(elem.grad().defined())
                {
                    
                    torch::autograd::Variable normWeight=elem.grad();
                    if(at::isnan(normWeight).any().item<int>())
                    {
                        std::cout<<"nan problem grad"<<std::endl;
                        nanProblem=true;
                    }
                }
            });
            std::for_each(param.begin(),param.end(),[&nanProblem](auto elem)
            {
                torch::autograd::Variable normWeight=elem;
                if(at::isnan(normWeight).any().item<int>())
                {
                    std::cout<<"nan problem"<<std::endl;
                    nanProblem=true;
                }
            });
            if(nanProblem)
            {
                std::for_each(copyStack.first.begin(),copyStack.first.end(),[](auto elem)
                {
                    std::cout<<elem<<std::endl;
                });
                whichAdam->zero_grad();
                auto errorvar=Graph.run<Point3d>(copyStack.first);
                Backward(errorvar,one);
                std::for_each(param.begin(),param.end(),[](auto elem)
                {
                if(elem.grad().defined())
                {
                    
                    torch::autograd::Variable normWeight=elem.grad();
                    if(at::isnan(normWeight).any().item<int>())
                    {
                        std::cout<<"nan problem grad repeat"<<std::endl;
                    }
                }
                });
                std::for_each(param.begin(),param.end(),[](auto elem)
                {
                torch::autograd::Variable normWeight=elem;
                if(at::isnan(normWeight).any().item<int>())
                {
                    std::cout<<"nan problem repeat"<<std::endl;
                }
                });
            }
            whichAdam->step();
            error=errorvar.item<double>();
            std::cout<<"epoch "<<i<<std::endl;
            std::cout<<"error "<<error<<std::endl;
            std::cout<<"weightNorm "<<weightNorm<<std::endl;
            
        }
    }
    catch(boost::python::error_already_set const &)
    {
        PyErr_Print();
    }
}
