#include <iostream>
#include <gtest/gtest.h>
#include <algorithm>
#include <pcl/io/vtk_lib_io.h>
#include <pcl/common/transforms.h>
#include <vtkVersion.h>
#include <vtkTriangle.h>
#include <vtkTriangleFilter.h>
#include <vtkPolyDataMapper.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/ply_io.h>

#include "Core/Sample.h"

#include <future>

#include <memory>

TEST(PCL,ReadAndSample)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_xyz (new pcl::PointCloud<pcl::PointXYZ>);
    Sample("/Data/Data/PointCloud/ModelNet40/01_xbox/train/xbox_0001.off.ply", cloud_xyz,500);
    pcl::PLYWriter writer;
    writer.write("/Data/Result/Test2.ply",*cloud_xyz);
}

/*TEST(PCL,ParallelReadAndSample)
{
    std::cout<<"start sampling"<<std::endl;
    using namespace pcl;
    using namespace pcl::io;
    using namespace pcl::console;

    const float default_leaf_size = 0.01f;
    std::cout<<"bef PLY"<<std::endl;
    pcl::PolygonMesh mesh;
    pcl::io::loadPolygonFilePLY ("/Data/Data/PointCloud/ModelNet40/bed/train/bed_0005.ply", mesh);
    std::cout<<"load done"<<std::endl;
    vtkSmartPointer<vtkPolyData> polydata1 = vtkSmartPointer<vtkPolyData>::New();
    pcl::io::mesh2vtk (mesh, polydata1);
    std::cout<<"mesh2vtk done"<<std::endl;
    //make sure that the polygons are triangles!
    vtkSmartPointer<vtkTriangleFilter> triangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
    triangleFilter->SetInput (polydata1);
    std::cout<<"Set input done"<<std::endl;
    triangleFilter->Update ();
    std::cout<<"Update done"<<std::endl;
    
    vtkSmartPointer<vtkPolyDataMapper> triangleMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
    triangleMapper->SetInputConnection (triangleFilter->GetOutputPort ());
    triangleMapper->Update ();
    polydata1 = triangleMapper->GetInput ();
    
    auto func=[&polydata1]()
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloudResult (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud_1 (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        std::cout<<"bef sampling"<<std::endl;
        uniform_sampling(polydata1, 1000, *cloud_1);
        std::cout<<"aft sampling"<<std::endl;
        // Voxelgrid
        VoxelGrid<PointXYZRGBNormal> grid_;
        grid_.setInputCloud (cloud_1);
        grid_.setLeafSize (0.01f, 0.01f, 0.01f);
    
        std::cout<<"aft leafSize"<<std::endl;

        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr voxel_cloud (new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        grid_.filter (*voxel_cloud);
        std::cout<<"aft filter"<<std::endl;
        // Strip uninitialized normals and colors from cloud:
        pcl::copyPointCloud (*voxel_cloud, *cloudResult);
        std::cout<<"done Sample"<<std::endl;
    };
    
    auto input=std::async(std::launch::async,func);
    auto input2=std::async(std::launch::async,func);
    input.wait();
    input2.wait();
}*/
